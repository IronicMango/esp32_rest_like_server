
// For keeping passwords out of main file
#include "officeLightingSecrets.h"

// These imports are for lighting control
#include <Adafruit_NeoPixel.h>
#ifdef __AVR__
  #include <avr/power.h>
#endif
#define PIN        27
#define NUMPIXELS 300

// These imports are for control using the web and wifi connection
#include <ArduinoJson.h>
#include <WebServer.h>
#include <WiFi.h>

// These imports are for OTA update capability
#include <ArduinoOTA.h>
#include <ESPmDNS.h>
#include <WiFiUdp.h>

// Initializing the Light strip
Adafruit_NeoPixel pixels(NUMPIXELS, PIN, NEO_GRB + NEO_KHZ800);

// mode_value will be the lighting mode that is selected. It will be referenced across functions
// The change_canary lets the program know if it's the first time through a particular mode, and detect when the mode has been changed
int mode_value;
int change_canary;

// Wifi credentials
// This must be a 2.4G network as the esp32 does not have 5G connecetivity
const char *SSID = SECRETS_SSID;
const char *PWD = SECRETS_PASSWORD;

// Keep track of the naming of the lighting modes
char *lighting_names[5] = {"off", "Pandora", "All On", "Purple Wave", "Forest Space"};

// Default port for a server
WebServer server(80);

// setting up a JSON doc for sending data
StaticJsonDocument<250> jsonDocument;
char buffer[250];

// Sets up URLs for accessing specific functionality
// data is for GET requests, and led is for POST
void setup_routing() {         
  server.on("/data", getData);     
  server.on("/led", HTTP_POST, handlePost);    
          
  server.begin();    
}

// Framework for JSON document to send
void create_json(char *tag, float value, char *unit) {  
  jsonDocument.clear();  
  jsonDocument["type"] = tag;
  jsonDocument["value"] = value;
  jsonDocument["unit"] = unit;
  serializeJson(jsonDocument, buffer);
}

void add_json_object(char *tag, float value, char *unit) {
  JsonObject obj = jsonDocument.createNestedObject();
  obj["type"] = tag;
  obj["value"] = value;
  obj["unit"] = unit; 
}

// Doesn't do much yet. Will need to add more data to send soon
void getData() {
  Serial.println("Get data");
  jsonDocument.clear();
  add_json_object("Lighting", mode_value, lighting_names[mode_value]);
  serializeJson(jsonDocument, buffer);
  server.send(200, "application/json", buffer);
}

// This function controls the receiving of POST requests to change the lighting.
// Command line requests should follow this format:
// curl -H "Content-Type: application/json" -X POST -d '{"mode":1}' http://{URL or IP address}/led
void handlePost() {
  if (server.hasArg("plain") == false) {
  }
  String body = server.arg("plain");
  deserializeJson(jsonDocument, body);

  // Change has been made so initialize change_canary
  change_canary = 0;
  Serial.println(change_canary);
  
  // This checks if there is a mode assigned to the number sent. If not no change is made
  if (jsonDocument["mode"] < (sizeof(lighting_names)/sizeof(lighting_names[0]))) {
     server.send(200, "application/json", "{Success!}");
     Serial.println(mode_value);
     mode_value = jsonDocument["mode"];
  } else {
     server.send(404, "application/json", "{Error: 404}");
     // Maybe put an error mode in here?
  }
}



// Runs continuously to keep the lights going
void run_lights(void * parameter) {
  for (;;) {
    if (mode_value == 0) {
      noLights();
    } else if (mode_value == 1) {
      pandoraWave();
    } else if (mode_value == 2) {
      lightsOn();
    } else if (mode_value == 3) {
      purpleWave();
    } else if (mode_value == 4) {
      forestSpace();
    }
    if (WiFi.status() != WL_CONNECTED) {
      reconnectWiFi();
    }
    // Pause the task again for 500ms
    vTaskDelay(500 / portTICK_PERIOD_MS);
  }
}


// This set's up a task for running the lights continuously
// The esp32 struggles with NeoPixels so running this intentionally on the second core (core 1) using FreeRTOS helps resolve issues
void setup_lights() {
  xTaskCreatePinnedToCore(
    run_lights,               // Function that should be called
    "Continuous Lighting",    // Name of the task (for debugging)
    1500,                     // Stack size (bytes)
    NULL,                     // Parameter to pass
    1,                        // Task priority
    NULL,                     // Task handle
    1                         // Core to run on (0 or 1)
  );
}

void reconnectWiFi() {
    // Connect to Wifi
  Serial.print("Connecting to Wi-Fi");
  WiFi.begin(SSID, PWD);
  while (WiFi.status() != WL_CONNECTED) {
    Serial.print(".");
    delay(500);
  }
 
  Serial.print("Connected! IP Address: ");
  Serial.println(WiFi.localIP());
}

void setup() {
  Serial.begin(115200);
  

  // A "just in case" for lighting. Could probably be removed.
  #if defined(__AVR_ATtiny85__) && (F_CPU == 16000000)
    clock_prescale_set(clock_div_1);
  #endif

  // Start the pixels listening for commands
  pixels.begin();

  // Connect to Wifi
  Serial.print("Connecting to Wi-Fi");
  WiFi.begin(SSID, PWD);
  while (WiFi.status() != WL_CONNECTED) {
    Serial.print(".");
    delay(500);
  }
 
  Serial.print("Connected! IP Address: ");
  Serial.println(WiFi.localIP());
  
  // This sets up OTA updates

  // Hostname defaults to esp3232-[MAC]
  ArduinoOTA.setHostname("RopeLights");

  // Set's the password by feeding in a MD5 hash
  ArduinoOTA.setPasswordHash(OTA_MD5_HASH);

  ArduinoOTA
    .onStart([]() {
      String type;
      if (ArduinoOTA.getCommand() == U_FLASH)
        type = "sketch";
      else // U_SPIFFS
        type = "filesystem";

      // NOTE: if updating SPIFFS this would be the place to unmount SPIFFS using SPIFFS.end()
      Serial.println("Start updating " + type);
    })
    .onEnd([]() {
      Serial.println("\nEnd");
    })
    .onProgress([](unsigned int progress, unsigned int total) {
      Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
    })
    .onError([](ota_error_t error) {
      Serial.printf("Error[%u]: ", error);
      if (error == OTA_AUTH_ERROR) Serial.println("Auth Failed");
      else if (error == OTA_BEGIN_ERROR) Serial.println("Begin Failed");
      else if (error == OTA_CONNECT_ERROR) Serial.println("Connect Failed");
      else if (error == OTA_RECEIVE_ERROR) Serial.println("Receive Failed");
      else if (error == OTA_END_ERROR) Serial.println("End Failed");
    });

  ArduinoOTA.begin();
  
  // This set's up the tasks for FreeRTOS. In this case the lights on core 1 and routing on whatever core is available. 
  setup_lights();  
  setup_routing(); 
}


// Just runs the server and checks for updates
void loop() {
  server.handleClient();
  ArduinoOTA.handle();
}


// Past this point is all of the lighting modes.
// --------------------------------------------

// Lights off
void noLights() {
    if (change_canary == 0){
    pixels.clear();
    change_canary = 1;
    delay(20);
    pixels.show();
    }

}

// Lights the whole rope a blue green with occasional pulse and higlights
void pandoraWave() {
  // Creates smooth starting animation to solid color.
  if (change_canary == 0){
    pixels.clear();
    for (int h=1; h<25; h++) {
      pixels.fill(pixels.Color(0,h/2,h),0);
      pixels.show();
      delay(30);
    }
    change_canary = 1;
  }

  // Looping animation

  //Random number for pulses
  int random_section_1 = random(0,301);
  int random_section_2 = random(0,301);
  int section_width = 30;
  for(int i=0; i<NUMPIXELS; i++) {
    // Pulsing of random section to more greenish brighter
    if (i <= NUMPIXELS/2) {
      int gAdd = i/2;
      int bAdd = i/4;
      // Section 1
      pixels.setPixelColor(random_section_1, pixels.Color(0,12+gAdd,25+bAdd));
      for (int j=1; j<section_width; j++) {
        if (random_section_1 + j <= 300){
          pixels.setPixelColor(random_section_1+j, pixels.Color(0,12+gAdd,25+bAdd));
          // pixels.setPixelColor(random_section_1+j, pixels.Color(0,12+(gAdd*((section_width-j)/section_width)),25+(bAdd*((section_width-j)/section_width))));
        }
        if (random_section_1 - j >= 0){
          pixels.setPixelColor(random_section_1-j, pixels.Color(0,12+gAdd,25+bAdd));
          // pixels.setPixelColor(random_section_1-j, pixels.Color(0,12+(gAdd*((section_width-j)/section_width)),25+(bAdd*((section_width-j)/section_width))));
        }
      }
      // Section 2
      pixels.setPixelColor(random_section_2, pixels.Color(0,12+gAdd,25+bAdd));
      for (int j=1; j<section_width; j++) {
        if (random_section_2 + j <= 300){
          pixels.setPixelColor(random_section_2+j, pixels.Color(0,12+gAdd,25+bAdd));
          // pixels.setPixelColor(random_section_2+j, pixels.Color(0,12+(gAdd*((section_width-j)/section_width)),25+(bAdd*((section_width-j)/section_width))));
        }
        if (random_section_2 - j >=0){
          pixels.setPixelColor(random_section_2-j, pixels.Color(0,12+gAdd,25+bAdd));
          // pixels.setPixelColor(random_section_2-j, pixels.Color(0,12+(gAdd*((section_width-j)/section_width)),25+(bAdd*((section_width-j)/section_width))));
        }
      }
    }
    if (i > NUMPIXELS/2) {
      int gAdd = (150/2) - (i-150)/2;
      int bAdd = (150/4) - (i-150)/4;
      // Section 1
      pixels.setPixelColor(random_section_1, pixels.Color(0,12+gAdd,25+bAdd));
      for (int j=1; j<section_width; j++) {
        if (random_section_1 + j <= 300){
          pixels.setPixelColor(random_section_1+j, pixels.Color(0,12+gAdd,25+bAdd));
          // pixels.setPixelColor(random_section_1+j, pixels.Color(0,12+(gAdd*((section_width-j)/section_width)),25+(bAdd*((section_width-j)/section_width))));
        }
        if (random_section_1 - j >=0){
          pixels.setPixelColor(random_section_1-j, pixels.Color(0,12+gAdd,25+bAdd));
          // pixels.setPixelColor(random_section_1-j, pixels.Color(0,12+(gAdd*((section_width-j)/section_width)),25+(bAdd*((section_width-j)/section_width))));
        }
      }
      // Section 2
      pixels.setPixelColor(random_section_2, pixels.Color(0,12+gAdd,25+bAdd));
      for (int j=1; j<section_width; j++) {
        if (random_section_2 + j <= 300){
          pixels.setPixelColor(random_section_2+j, pixels.Color(0,12+gAdd,25+bAdd));
          // pixels.setPixelColor(random_section_2+j, pixels.Color(0,12+(gAdd*((section_width-j)/section_width)),25+(bAdd*((section_width-j)/section_width))));
        }
        if (random_section_2 - j >=0){
          pixels.setPixelColor(random_section_2-j, pixels.Color(0,12+gAdd,25+bAdd));
          // pixels.setPixelColor(random_section_2-j, pixels.Color(0,12+(gAdd*((section_width-j)/section_width)),25+(bAdd*((section_width-j)/section_width))));
        }
      }
    }
    pixels.setPixelColor(i, pixels.Color(0, 110, 220));
    if (i > 6){
      pixels.setPixelColor(i-1, pixels.Color(0, 85, 170));
      pixels.setPixelColor(i-2, pixels.Color(0, 60, 120));
      pixels.setPixelColor(i-3, pixels.Color(0, 50, 100));
      pixels.setPixelColor(i-4, pixels.Color(0, 25, 50));
      pixels.setPixelColor(i-5, pixels.Color(0, 12, 25));
    }
    pixels.show();
    delay(20);
    // break the loop to change the lighting mode
    if (change_canary == 0) {
      break;
    }
  }
}

// Lights on plain white
void lightsOn() {
  if (change_canary == 0) {
    for(int i=0; i<NUMPIXELS; i++) {
      pixels.setPixelColor(i, pixels.Color(155,155,155));
    }
    pixels.show();
    change_canary = 1;
  }
  delay(20);
}

// Lights the whole rope a blue green with purple effects
void purpleWave() {
  // Creates smooth starting animation to solid color.
  if (change_canary == 0){
    pixels.clear();
    for (int h=1; h<25; h++) {
      pixels.fill(pixels.Color(0,h/2,h),0);
      pixels.show();
      delay(30);
    }
    change_canary = 1;
  }

  // Looping animation

  //Random number for pulses
  int random_section_1 = random(0,301);
  int random_section_2 = random(0,301);
  int section_width = 30;
  for(int i=0; i<NUMPIXELS; i++) {
    // Pulsing of random section to more greenish brighter
    if (i <= NUMPIXELS/2) {
      int rAdd = i/3;
      int bAdd = i/2;
      // Section 1
      pixels.setPixelColor(random_section_1, pixels.Color(rAdd,12,25+bAdd));
      for (int j=1; j<section_width; j++) {
        if (random_section_1 + j <= 300){
          pixels.setPixelColor(random_section_1+j, pixels.Color(rAdd,12,25+bAdd));
          // pixels.setPixelColor(random_section_1+j, pixels.Color(0,12+(gAdd*((section_width-j)/section_width)),25+(bAdd*((section_width-j)/section_width))));
        }
        if (random_section_1 - j >= 0){
          pixels.setPixelColor(random_section_1-j, pixels.Color(rAdd,12,25+bAdd));
          // pixels.setPixelColor(random_section_1-j, pixels.Color(0,12+(gAdd*((section_width-j)/section_width)),25+(bAdd*((section_width-j)/section_width))));
        }
      }
      // Section 2
      pixels.setPixelColor(random_section_2, pixels.Color(rAdd,12,25+bAdd));
      for (int j=1; j<section_width; j++) {
        if (random_section_2 + j <= 300){
          pixels.setPixelColor(random_section_2+j, pixels.Color(rAdd,12,25+bAdd));
          // pixels.setPixelColor(random_section_2+j, pixels.Color(0,12+(gAdd*((section_width-j)/section_width)),25+(bAdd*((section_width-j)/section_width))));
        }
        if (random_section_2 - j >=0){
          pixels.setPixelColor(random_section_2-j, pixels.Color(rAdd,12,25+bAdd));
          // pixels.setPixelColor(random_section_2-j, pixels.Color(0,12+(gAdd*((section_width-j)/section_width)),25+(bAdd*((section_width-j)/section_width))));
        }
      }
    }
    if (i > NUMPIXELS/2) {
      int rAdd = (150/3) - (i-150)/3;
      int bAdd = (150/2) - (i-150)/2;
      // Section 1
      pixels.setPixelColor(random_section_1, pixels.Color(rAdd,12,25+bAdd));
      for (int j=1; j<section_width; j++) {
        if (random_section_1 + j <= 300){
          pixels.setPixelColor(random_section_1+j, pixels.Color(rAdd,12,25+bAdd));
          // pixels.setPixelColor(random_section_1+j, pixels.Color(0,12+(gAdd*((section_width-j)/section_width)),25+(bAdd*((section_width-j)/section_width))));
        }
        if (random_section_1 - j >=0){
          pixels.setPixelColor(random_section_1-j, pixels.Color(rAdd,12,25+bAdd));
          // pixels.setPixelColor(random_section_1-j, pixels.Color(0,12+(gAdd*((section_width-j)/section_width)),25+(bAdd*((section_width-j)/section_width))));
        }
      }
      // Section 2
      pixels.setPixelColor(random_section_2, pixels.Color(rAdd,12,25+bAdd));
      for (int j=1; j<section_width; j++) {
        if (random_section_2 + j <= 300){
          pixels.setPixelColor(random_section_2+j, pixels.Color(rAdd,12,25+bAdd));
          // pixels.setPixelColor(random_section_2+j, pixels.Color(0,12+(gAdd*((section_width-j)/section_width)),25+(bAdd*((section_width-j)/section_width))));
        }
        if (random_section_2 - j >=0){
          pixels.setPixelColor(random_section_2-j, pixels.Color(rAdd,12,25+bAdd));
          // pixels.setPixelColor(random_section_2-j, pixels.Color(0,12+(gAdd*((section_width-j)/section_width)),25+(bAdd*((section_width-j)/section_width))));
        }
      }
    }
    pixels.setPixelColor(i, pixels.Color(0, 110, 220));
    if (i > 6){
      pixels.setPixelColor(i-1, pixels.Color(0, 85, 170));
      pixels.setPixelColor(i-2, pixels.Color(0, 60, 120));
      pixels.setPixelColor(i-3, pixels.Color(0, 50, 100));
      pixels.setPixelColor(i-4, pixels.Color(0, 25, 50));
      pixels.setPixelColor(i-5, pixels.Color(0, 12, 25));
    }
    pixels.show();
    delay(20);
    // break the loop to change the lighting mode
    if (change_canary == 0) {
      break;
    }
  }
}


// CURRENTLY INCOMPLETE
void fire() {
  struct RGB {
    byte r;
    byte g;
    byte b;
  };
  
  //  The flame color array
  RGB flameColors[] = {
    { 226, 60, 0},  // Orange flame
    { 226, 30, 0}    // Red flame
    };
    
  //  Flicker, based on our initial RGB values
  for(int i=0; i<pixels.numPixels(); i++) {
    RGB currentColor = flameColors[random(0,2)];
    int flicker = random(0,55);
    int r1 = currentColor.r-flicker;
    int g1 = currentColor.g-flicker;
    int b1 = currentColor.b-flicker;
    if(g1<0) g1=0;
    if(r1<0) r1=0;
    if(b1<0) b1=0;
    pixels.setPixelColor(i,r1,g1, b1);
  }
  pixels.show();

  //  Adjust the delay here, if you'd like.  Right now, it randomizes the 
  //  color switch delay to give a sense of realism
  delay(random(10,113));
}


// CURRENTLY INCOMPLETE
void forestSpace() {
  // Creates smooth starting animation to solid color with highlights.
  if (change_canary == 0){
    pixels.clear();
    for (int h=1; h<25; h++) {
      pixels.fill(pixels.Color(0,h,0),0);
      pixels.show();
      delay(30);
    }
    change_canary = 1;
  }

  // Looping animation

  //Random number for pulses
  int random_section_1 = random(0,301);
  int random_section_2 = random(0,301);
  int section_width = 30;
  for(int i=0; i<NUMPIXELS; i++) {
    // Pulsing of random section to more greenish brighter
    if (i <= NUMPIXELS/2) {
      int rAdd = i/3;
      int bAdd = i/2;
      // Section 1
      pixels.setPixelColor(random_section_1, pixels.Color(rAdd,12,25+bAdd));
      for (int j=1; j<section_width; j++) {
        if (random_section_1 + j <= 300){
          pixels.setPixelColor(random_section_1+j, pixels.Color(rAdd,12,25+bAdd));
          // pixels.setPixelColor(random_section_1+j, pixels.Color(0,12+(gAdd*((section_width-j)/section_width)),25+(bAdd*((section_width-j)/section_width))));
        }
        if (random_section_1 - j >= 0){
          pixels.setPixelColor(random_section_1-j, pixels.Color(rAdd,12,25+bAdd));
          // pixels.setPixelColor(random_section_1-j, pixels.Color(0,12+(gAdd*((section_width-j)/section_width)),25+(bAdd*((section_width-j)/section_width))));
        }
      }
      // Section 2
      pixels.setPixelColor(random_section_2, pixels.Color(rAdd,12,25+bAdd));
      for (int j=1; j<section_width; j++) {
        if (random_section_2 + j <= 300){
          pixels.setPixelColor(random_section_2+j, pixels.Color(rAdd,12,25+bAdd));
          // pixels.setPixelColor(random_section_2+j, pixels.Color(0,12+(gAdd*((section_width-j)/section_width)),25+(bAdd*((section_width-j)/section_width))));
        }
        if (random_section_2 - j >=0){
          pixels.setPixelColor(random_section_2-j, pixels.Color(rAdd,12,25+bAdd));
          // pixels.setPixelColor(random_section_2-j, pixels.Color(0,12+(gAdd*((section_width-j)/section_width)),25+(bAdd*((section_width-j)/section_width))));
        }
      }
    }
    if (i > NUMPIXELS/2) {
      int rAdd = (150/3) - (i-150)/3;
      int bAdd = (150/2) - (i-150)/2;
      // Section 1
      pixels.setPixelColor(random_section_1, pixels.Color(rAdd,12,25+bAdd));
      for (int j=1; j<section_width; j++) {
        if (random_section_1 + j <= 300){
          pixels.setPixelColor(random_section_1+j, pixels.Color(rAdd,12,25+bAdd));
          // pixels.setPixelColor(random_section_1+j, pixels.Color(0,12+(gAdd*((section_width-j)/section_width)),25+(bAdd*((section_width-j)/section_width))));
        }
        if (random_section_1 - j >=0){
          pixels.setPixelColor(random_section_1-j, pixels.Color(rAdd,12,25+bAdd));
          // pixels.setPixelColor(random_section_1-j, pixels.Color(0,12+(gAdd*((section_width-j)/section_width)),25+(bAdd*((section_width-j)/section_width))));
        }
      }
      // Section 2
      pixels.setPixelColor(random_section_2, pixels.Color(rAdd,12,25+bAdd));
      for (int j=1; j<section_width; j++) {
        if (random_section_2 + j <= 300){
          pixels.setPixelColor(random_section_2+j, pixels.Color(rAdd,12,25+bAdd));
          // pixels.setPixelColor(random_section_2+j, pixels.Color(0,12+(gAdd*((section_width-j)/section_width)),25+(bAdd*((section_width-j)/section_width))));
        }
        if (random_section_2 - j >=0){
          pixels.setPixelColor(random_section_2-j, pixels.Color(rAdd,12,25+bAdd));
          // pixels.setPixelColor(random_section_2-j, pixels.Color(0,12+(gAdd*((section_width-j)/section_width)),25+(bAdd*((section_width-j)/section_width))));
        }
      }
    }
    pixels.setPixelColor(i, pixels.Color(0, 110, 220));
    if (i > 6){
      pixels.setPixelColor(i-1, pixels.Color(0, 85, 170));
      pixels.setPixelColor(i-2, pixels.Color(0, 60, 120));
      pixels.setPixelColor(i-3, pixels.Color(0, 50, 100));
      pixels.setPixelColor(i-4, pixels.Color(0, 25, 50));
      pixels.setPixelColor(i-5, pixels.Color(0, 12, 25));
    }
    pixels.show();
    delay(20);
    // break the loop to change the lighting mode
    if (change_canary == 0) {
      break;
    }
  }
}
